﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LicenseManage.Controllers
{
    public class LicenseController : Controller
    {
        string DBConnection = System.Configuration.ConfigurationManager.ConnectionStrings["LocalMySQL"].ConnectionString;
        // GET: License
        [HttpPost]
        public ContentResult Check(string MAC, string IP)
        {
            DateTime dt = DateTime.MinValue;
            using (MySqlConnection conn = new MySqlConnection(DBConnection))
            {
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select ExpireDate from securitylicense where mac=?mac and ip=?ip";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("?mac", MAC);
                cmd.Parameters.AddWithValue("?ip", IP);
                try
                {
                    conn.Open();
                    var obj = cmd.ExecuteScalar();
                    if (obj != null)
                        dt = DateTime.Parse(obj.ToString());
                }
                catch
                {
                    throw;
                }
            }
            return Content(dt.ToShortDateString());
        }
        [HttpPost]
        public ContentResult Regist(string MAC, string IP)
        {
            using (MySqlConnection conn = new MySqlConnection(DBConnection))
            {
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "registClient";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("?macAdd", MAC);
                cmd.Parameters.AddWithValue("?ipAdd", IP);
                try
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    throw;
                }
            }
            return Content("");
        }
    }
}