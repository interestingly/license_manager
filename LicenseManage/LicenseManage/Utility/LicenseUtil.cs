﻿using System;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace LicenseManage.Utility
{
    class LicenseUtil
    {
        public static DateTime getExpireDate()
        {
            var netcards = NetworkInterface.GetAllNetworkInterfaces();
            string mac = "";
            string ip = "";
            foreach (var card in netcards)
            {
                if (card.OperationalStatus == OperationalStatus.Up)
                {
                    mac = card.GetPhysicalAddress().ToString();
                    break;
                }
            }
            string hostname = Dns.GetHostName();
            var addresses = Dns.GetHostAddresses(hostname);
            foreach (var addr in addresses)
            {
                if (addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    ip = addr.ToString();
                    break;
                }
            }
            WebRequest request = WebRequest.Create("http://www.tigger.top/License/Check");
            string postData = string.Format("MAC={0}&IP={1}", System.Web.HttpUtility.UrlEncode(mac), System.Web.HttpUtility.UrlEncode(ip));
            byte[] data = Encoding.UTF8.GetBytes(postData);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            request.ContentLength = data.Length;
            var newStream = request.GetRequestStream();
            newStream.Write(data, 0, data.Length);
            newStream.Close();
            HttpWebResponse myResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
            string content = reader.ReadToEnd();
            reader.Close();
            return DateTime.Parse(content);

        }

        public static void RegistClient()
        {
            var netcards = NetworkInterface.GetAllNetworkInterfaces();
            string mac = "";
            string ip = "";
            foreach (var card in netcards)
            {
                if (card.OperationalStatus == OperationalStatus.Up)
                {
                    mac = card.GetPhysicalAddress().ToString();
                    break;
                }
            }
            string hostname = Dns.GetHostName();
            var addresses = Dns.GetHostAddresses(hostname);
            foreach (var addr in addresses)
            {
                if (addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    ip = addr.ToString();
                    break;
                }
            }
            WebRequest request = WebRequest.Create("http://www.tigger.top/License/Regist");
            string postData = string.Format("MAC={0}&IP={1}", System.Web.HttpUtility.UrlEncode(mac), System.Web.HttpUtility.UrlEncode(ip));
            byte[] data = Encoding.UTF8.GetBytes(postData);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            request.ContentLength = data.Length;
            var newStream = request.GetRequestStream();
            newStream.Write(data, 0, data.Length);
            newStream.Close();
            HttpWebResponse myResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
            string content = reader.ReadToEnd();
            reader.Close();
        }
    }
}