﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperMap.Models
{
    public class Form
    {
        public string submiter { get; set; }
        public string tel { get; set; }
        public string village { get; set; }
        public string worktime { get; set; }
        public string backtime { get; set; }
        public string start { get; set; }

        public string back { get; set; }

        public Point startpoi { get; set; }

        public Point backpoi { get; set; }

    }

    public class Point
    {
        public decimal lon { get; set; }
        public decimal lat { get; set; }
    }
}