﻿


function PoiSearch(keyword,callback ,url) {
    PoiStyle = {
        graphic: true,
        externalGraphic: "/Content/images/poimarker.png",
        graphicWidth: 20,
        graphicHeight: 25,
        graphicYOffset:-20
    }

    vectorLayer = null;
    if (map.getLayersByName("poiSearch").length == 1) {
        vectorLayer = map.getLayersByName("poiSearch")[0];
        vectorLayer.removeAllFeatures();
    }
    else {
        vectorLayer = new SuperMap.Layer.Vector("poiSearch");
        map.addLayer(vectorLayer);
    }

    vectorLayer.removeAllFeatures();

    var queryParams=[], queryBySQLParams, queryBySQLService;
    queryParams[0] = new SuperMap.REST.FilterParameter({
        name: "Airport_pt@China.1",
        attributeFilter: "Name like '%" + keyword + "%'"
    });
    queryBySQLParams = new SuperMap.REST.QueryBySQLParameters({
        queryParams: queryParams
    });
    queryBySQLService = new SuperMap.REST.QueryBySQLService(url, {
        eventListeners: {
            "processCompleted":callback?callback:processCompleted, "processFailed": function processFailed(e) {
                alert(e.error.errorMsg);
            }
        }
    });
    queryBySQLService.processAsync(queryBySQLParams);
}

function processCompleted(queryEventArgs) {
    var i, j, feature,
        result = queryEventArgs.result;
    if (result && result.recordsets) {
        for (i = 0; i < result.recordsets.length; i++) {
            if (result.recordsets[i].features) {
                for (j = 0; j < result.recordsets[i].features.length; j++) {
                    feature = result.recordsets[i].features[j];
                    feature.style = PoiStyle;
                    vectorLayer.addFeatures(feature);
                }
            }
        }
    }
}