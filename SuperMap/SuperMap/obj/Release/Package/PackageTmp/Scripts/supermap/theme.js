﻿function addThemeUnique(url,display) {
    var themeLayer = null;
    if (map.getLayersByName("区县图").length == 1) {
        themeLayer = map.getLayersByName("区县图")[0];
        map.removeLayer(themeLayer, true);
        if (display == false) {
            return;
        }

    }
    var themeService = new SuperMap.REST.ThemeService(url, {
        eventListeners: {
            "processCompleted": function (themeEventArgs) {
                if (themeEventArgs.result.resourceInfo.id) {
                    themeLayer = new SuperMap.Layer.TiledDynamicRESTLayer("区县图", url, {
                        cacheEnabled: false,
                        transparent: true,
                        layersID: themeEventArgs.result.resourceInfo.id
                    }, { "maxResolution": "auto" });
                    themeLayer.events.on({
                        "layerInitialized": function() {
                            map.addLayer(themeLayer);
                            themeLayer.setOpacity(0.5)
                        }
                    });
                }
            },
            "processFailed": function (serviceFailedEventArgs) {
                    //doMapAlert("",serviceFailedEventArgs.error.errorMsg,true);
                widgets.alert.showAlert(serviceFailedEventArgs.error.errorMsg, false);
            }
        }
    });
    var style1, style2, style3, style4, style5, style6;
    style1 = new SuperMap.REST.ServerStyle({
        fillForeColor: new SuperMap.REST.ServerColor(248, 203, 249),
        lineColor: new SuperMap.REST.ServerColor(0, 0, 0),
        lineWidth: 0.1,
    });
    style2 = new SuperMap.REST.ServerStyle({
        fillForeColor: new SuperMap.REST.ServerColor(196, 255, 189),
        lineColor: new SuperMap.REST.ServerColor(0, 0, 0),
        lineWidth: 0.1,
    });
    style3 = new SuperMap.REST.ServerStyle({
        fillForeColor: new SuperMap.REST.ServerColor(255, 173, 173),
        lineColor: new SuperMap.REST.ServerColor(0, 0, 0),
        lineWidth: 0.1,
    });
    style4 = new SuperMap.REST.ServerStyle({
        fillForeColor: new SuperMap.REST.ServerColor(255, 239, 168),
        lineColor: new SuperMap.REST.ServerColor(0, 0, 0),
        lineWidth: 0.1,
    });



    var themeUniqueIteme1 = new SuperMap.REST.ThemeUniqueItem({
        style: style1
    }),
        themeUniqueIteme2 = new SuperMap.REST.ThemeUniqueItem({
            style: style2
        }),
        themeUniqueIteme3 = new SuperMap.REST.ThemeUniqueItem({
            style: style3
        }),
        themeUniqueIteme4 = new SuperMap.REST.ThemeUniqueItem({
            style: style4
        });


    var themeUniqueItemes = [themeUniqueIteme1, themeUniqueIteme2, themeUniqueIteme3, themeUniqueIteme4];

    var themeUnique = new SuperMap.REST.ThemeUnique({
        uniqueExpression: "行政区名",
        items: themeUniqueItemes,
        defaultStyle: style1
    });
    themeParameters = new SuperMap.REST.ThemeParameters({
        datasetNames: ["T2000_84_行政区2"],
        dataSourceNames: ["China"],
        themes: [themeUnique],
        types: ['REGION']
    });

    themeService.processAsync(themeParameters);
}
