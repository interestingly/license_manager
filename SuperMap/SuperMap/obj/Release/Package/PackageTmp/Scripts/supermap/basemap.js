﻿var map, baselayer, center;
var baseSpeed = 0.05;
function init(holder, url, c) {
    //初始化地图
    map = new SuperMap.Map(holder, {
        controls: [
            new SuperMap.Control.LayerSwitcher(),
            new SuperMap.Control.Navigation(),
            new SuperMap.Control.Zoom()],
        allOverlays: true
    });
    map.addControl(new SuperMap.Control.MousePosition());
    //初始化图层
    baselayer = new SuperMap.Layer.TiledDynamicRESTLayer("baseMap", url, null, { maxResolution: "auto" });
    if (c) {
        center = c;
    }
    //监听图层信息加载完成事件
    baselayer.events.on({
        "layerInitialized": function () {
            map.addLayer(baselayer);
            //显示地图范围
            map.setCenter(new SuperMap.LonLat(center.x, center.y), 8);
        }
    });
}


function switchLock() {
    var control = map.getControlsByClass("SuperMap.Control.Navigation")[0];
    if (control.handlers.wheel.active)
        control.handlers.wheel.deactivate();
    else
        control.handlers.wheel.activate();
}

function showHitMap(points, valuefieldName, radius) {
    radius = radius || 5;

    var heatMapLayer = null;
    lays = map.getLayersByName("heatMap");
    if (lays.length == 1)
        heatMapLayer = lays[0];
    else {
        heatMapLayer = new SuperMap.Layer.HeatMapLayer(
            "heatMap",
            {
                "radius": 45,
                "featureWeight": valuefieldName,
                "featureRadius": false
            }
        );
        map.addLayer(heatMapLayer);
    }
    heatPoints = [];
    heatMapLayer.removeAllFeatures();
    for (var i = 0; i < points.length; i++) {
        vector = new SuperMap.Feature.Vector(
            new SuperMap.Geometry.Point(points[i].x, points[i].y), points[i]);
        heatPoints.push(vector);
    }
    heatMapLayer.addFeatures(heatPoints);
    heatMapLayer.radius = radius;
}

function showMonitor(points) {
    //pointstyle = {
    //    fillColor: "red",
    //    strokeColor: "none",
    //    pointRadius: 2
    //};
    var empty = "/Content/images/emptycar.png";
    var busy = "/Content/images/busycar.png";
    var animatorVector = null;
    if (map.getLayersByName("Monitor").length == 1)
        animatorVector = map.getLayersByName("Monitor")[0];
    else {
        animatorVector = new SuperMap.Layer.Vector("Monitor");
        map.addLayer(animatorVector);
    }
    var vectors = [];
    var maxX=0, maxY=0, minX=999999999, minY=999999999;
    animatorVector.removeAllFeatures();
    for (var i = 0; i < points.length; i++) {
        var p = new SuperMap.Geometry.Point(points[i].X, points[i].Y);
        maxX = points[i].X > maxX ? points[i].X : maxX;
        maxY = points[i].Y > maxY ? points[i].Y : maxY;
        minX = points[i].X < minX ? points[i].X : minX;
        minY = points[i].Y < minY ? points[i].Y : minY;

        var v = new SuperMap.Feature.Vector(p, points[i], {
            graphic:true,
            externalGraphic: points[i].Status? busy:empty ,
            graphicWidth: 15,
            graphicHeight: 15
        });

        vectors.push(v);
    }
    animatorVector.addFeatures(vectors);
    var bound=new SuperMap.Bounds(minX,minY,maxX,maxY);
    map.zoomToExtent(bound, true);
}


function showReplay(path) {
    var animatorVector = null;
    var pathVector = null;
    linestyle = {
        strokeColor: "red",
        strokeWidth: 2,
        fill: false
    };
    var pointstyle =
        {
            graphic: true,
            externalGraphic: "/Content/images/busycar.png",
            graphicWidth: 15,
            graphicHeight: 15
        };
    if (map.getLayersByName("replayLayer").length == 1 && map.getLayersByName("pathLayer").length == 1) {
        animatorVector = map.getLayersByName("replayLayer")[0];
        pathVector = map.getLayersByName("pathLayer")[0];
    }
    else {
        animatorVector = new SuperMap.Layer.AnimatorVector("replayLayer", {}, {
            speed: baseSpeed,
            startTime: 0,
            repeat: false
        });
        pathVector = new SuperMap.Layer.Vector("pathLayer");
        map.addLayers([pathVector, animatorVector]);
    }
    var route = [];
    var points = [];
    var maxX = 0, maxY = 0, minX = 999999999, minY = 999999999;
    for (var i = 0; i < path.length; i++) {
        maxX = path[i].X > maxX ? path[i].X : maxX;
        maxY = path[i].Y > maxY ? path[i].Y : maxY;
        minX = path[i].X < minX ? path[i].X : minX;
        minY = path[i].Y < minY ? path[i].Y : minY;
        p = new SuperMap.Geometry.Point(path[i].X, path[i].Y)
        points.push(p);
        vector = new SuperMap.Feature.Vector(p.clone(), {
            FEATUREID: path[i].PlateNo,
            //根据节点生成时间
            TIME: path[i].Ticks,
            Extend: path[i]
        }, pointstyle);
        route.push(vector);
    }
    roadLine = new SuperMap.Geometry.LineString(points);
    trail = new SuperMap.Feature.Vector(roadLine, null, linestyle);
    pathVector.removeAllFeatures();
    animatorVector.removeAllFeatures();
    pathVector.addFeatures([trail]);
    animatorVector.addFeatures(route);
    animatorVector.animator.setStartTime(path[0].Ticks);
    animatorVector.animator.setSpeed(baseSpeed);
    animatorVector.animator.start();
    var bound = new SuperMap.Bounds(minX, minY, maxX, maxY);
    map.zoomToExtent(bound, true);
    return animatorVector;
}

function setSpeed(animator,n) {
    animator.animator.setSpeed( baseSpeed * n);
}

function PausePlay(animator) {
    animator.animator.pause();
}

function StopPlay(animator) {
    animator.animator.stop();
}


function appendLayer(name, url) {
    var layer = null;
    if (map.getLayersByName(name).length == 1) {
        layer = map.getLayersByName(name)[0];
        layer.url = url;
        layer.setVisibility(true);
    }
    else {
        layer = new SuperMap.Layer.TiledDynamicRESTLayer(name, url, { transparent: true, cacheEnabled: true });
        layer.setOpacity(1);
        layer.events.on({
            "layerInitialized": function () {
                map.addLayer(layer);
            }
        });
    }
}



function hideLayer(name) {
    var layer = null;
    if (map.getLayersByName(name).length == 1) {
        layer = map.getLayersByName(name)[0];
        map.removeLayer(layer,false)
    }
}

/**
 * url输出面板HTML
**/
function displayPanel(holderId, url) {
    $("#" + holderId).load(url);
}

function TiggerRails(rails) {
    var layer = null;
    var standStyle = {
        strokeColor: "#CAFF70",
        fillColor: "#C6E2FF",
        strokeWidth: 2,
        fillOpacity: 0.5
    };
    if (map.getLayersByName("rails").length == 1) {
        layer = map.getLayersByName("rails")[0];
    }
    else {
        layer = new SuperMap.Layer.Vector("rails");
        map.addLayer(layer);
    }
    var vectors = [];
    for (i = 0; i < rails.length; i++) {
        var label = rails[i].label;
        if (rails[i].type == "Circle")
        {
            var centerPoint = new SuperMap.Geometry.Point(rails[i].center.X, rails[i].center.Y);
            var circleP = createCircle(centerPoint, rails[i].radius, 256, 360, 360);
            var style = jQuery.extend(true, {}, standStyle);
            style.label = label;
            var circleVector = new SuperMap.Feature.Vector(circleP,null,style);
            vectors.push(circleVector);
        }
        else{
            var path = [];
            trail=rails[i].path;
            for (j = 0; j < trail.length; j++) {
                point = new SuperMap.Geometry.Point(trail[j].X, trail[j].Y);
                path.push(point);
            }
            linearRings = new SuperMap.Geometry.LinearRing(path);
            region = new SuperMap.Geometry.Polygon([linearRings]);
            var style = jQuery.extend(true, {}, standStyle);
            style.label = label;
            polygonVector = new SuperMap.Feature.Vector(region, null, style);
            vectors.push(polygonVector);
        }
    }
    layer.removeAllFeatures();
    layer.addFeatures(vectors);
    return layer;
}

function supportClick2GetLocation()
{
    layer = null;
    if (map.getLayersByName("test").length == 1) {
        layer = map.getLayersByName("test")[0];
    }
    else {
        layer = new SuperMap.Layer.Vector("test");
        map.addLayer(layer);
    }
    map.events.on({
        "click": function (args) {
            px = new SuperMap.Pixel(args.layerX, args.layerY);
            lnglat = map.getLonLatFromPixel(px);
            point = new SuperMap.Geometry.Point(lnglat.lon, lnglat.lat);
            console.log(lnglat);
            vector = new SuperMap.Feature.Vector(point);
            layer.addFeatures([vector]);
            //$.post("/Home/CollectPoints", { location: lnglat.toShortString() }, function (data) {
                
            //});
        
    }})
}


/*************************************************以下方法不提供外部调用*****************************************************/
function createCircle(origin, radius, sides, r, angel) {
    var rR = r * Math.PI / (180 * sides);
    var rotatedAngle, x, y;
    var points = [];
    for (var i = 0; i < sides; ++i) {
        rotatedAngle = rR * i;
        x = origin.x + (radius * Math.cos(rotatedAngle));
        y = origin.y + (radius * Math.sin(rotatedAngle));
        points.push(new SuperMap.Geometry.Point(x, y));
    }
    rotatedAngle = r * Math.PI / 180;
    x = origin.x + (radius * Math.cos(rotatedAngle));
    y = origin.y + (radius * Math.sin(rotatedAngle));
    points.push(new SuperMap.Geometry.Point(x, y));

    var ring = new SuperMap.Geometry.LinearRing(points);
    ring.rotate(parseFloat(angel), origin);
    var geo = new SuperMap.Geometry.Collection([ring]);
    geo.origin = origin;
    geo.radius = radius;
    geo.r = r;
    geo.angel = angel;
    geo.sides = sides;
    geo.polygonType = "Curve";
    return geo;
}

