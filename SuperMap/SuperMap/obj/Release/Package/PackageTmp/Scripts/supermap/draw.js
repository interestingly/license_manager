﻿var polygonpan, rectanglepan, circlepan, modifyPan;

function addDrawControllers() {
    vectorlayer = null;
    if (map.getLayersByName("DrawingLayer").length == 1)
        vectorlayer = map.getLayersByName("DrawingLayer")[0];
    else {
        vectorlayer = new SuperMap.Layer.Vector("DrawingLayer");
        map.addLayer(vectorlayer);
    }

    polygonpan = new SuperMap.Control.DrawFeature(vectorlayer, SuperMap.Handler.Polygon);
    rectanglepan = new SuperMap.Control.DrawFeature(vectorlayer, SuperMap.Handler.Box);
    circlepan = new SuperMap.Control.DrawFeature(vectorlayer, SuperMap.Handler.RegularPolygon, { handlerOptions: { sides: 360 } });
    modifyPan = new SuperMap.Control.ModifyFeature(vectorlayer);
    modifyPan.snap = new SuperMap.Snap([vectorlayer], 10, 10, { actived: false })
    map.addControls([polygonpan, rectanglepan, circlepan, modifyPan]);
}


function drawPolygon(callback) {
    disableDrawing();
    polygonpan.events.on({"featureadded":callback});
    polygonpan.activate();

}

function drawRectangle(callback) {
    disableDrawing()
    rectanglepan.events.on({"featureadded": callback});
    rectanglepan.activate();
}

function drawCircle(callback) {
    disableDrawing()
    circlepan.events.on({"featureadded": callback});
    circlepan.activate();
}

function disableDrawing() {
    polygonpan.deactivate();
    rectanglepan.deactivate();
    circlepan.deactivate();
    modifyPan.deactivate();
    modifyPan.snap.off();
}

function enbaleEdit(before,doing,after) {
    disableDrawing();
    modifyPan.events.on({"beforefeaturemodified": before});
    modifyPan.events.on({"featuremodified": doing});
    modifyPan.events.on({"afterfeaturemodified": after});
    modifyPan.activate();
    modifyPan.snap.on();
}