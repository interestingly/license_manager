﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;

namespace SuperMap.Utility
{
    public class Geocoder
    {
        private const string AK = "4xqGqqEnRXHiEdhdZFYQuWjM";
        private const string SK = "";
        private const string URL = "http://api.map.baidu.com/geocoder/v2/";
        public Models.Point Address2LngLat(string address)
        {
            using(var client= new HttpClient())
            {
                var values = new List<KeyValuePair<string, string>>();
                values.Add(new KeyValuePair<string, string>("address", address));
                values.Add(new KeyValuePair<string, string>("ak", AK));
                //var sn = AKSNCaculater.CaculateAKSN(AK, SK, URL, values);
                //values.Add(new KeyValuePair<string, string>("sn", sn));

                var post = new FormUrlEncodedContent(values);
                var response =  client.PostAsync(URL, post).Result;
                var content = response.Content.ReadAsStringAsync().Result;
                return new Models.Point();
            }
        }

        public string LngLat2Address(Models.Point lnglat)
        {
            using (var client = new HttpClient())
            {
                var values = new List<KeyValuePair<string, string>>();
                values.Add(new KeyValuePair<string, string>("location", string.Format("{0},{1}",lnglat.lat,lnglat.lon)));
                values.Add(new KeyValuePair<string, string>("coordtype", "wgs84ll"));
                values.Add(new KeyValuePair<string, string>("ak", AK));
                //var sn = AKSNCaculater.CaculateAKSN(AK, SK, URL, values);
                //values.Add(new KeyValuePair<string, string>("sn", sn));
                var post = new FormUrlEncodedContent(values);
                var response = client.PostAsync(URL, post).Result;
                var content = response.Content.ReadAsStringAsync().Result;
                return getAddress(content);
            }
        }

        private string getAddress(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            var node = doc.SelectSingleNode("/GeocoderSearchResponse/result/formatted_address");
            return node.InnerText;

        }

        //private Models.Point getLnglat(string xml)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.LoadXml(xml);
        //    var node = doc.SelectSingleNode("/GeocoderSearchResponse/result/formatted_address");
        //    return node.Value;
        //}
    }
}