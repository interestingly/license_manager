﻿var analystUrl = "http://120.76.199.62:8090/iserver/services/transportationanalyst-sample/rest/networkanalyst/RoadNet@Changchun";
var nodeArray = [], pathTime, pathListIndex = 0, routeCompsIndex = 0;
var analystLayer = null;
function routeSearch(stops) {
    var findPathService, parameter, analystParameter, resultSetting;
    resultSetting = new SuperMap.REST.TransportationAnalystResultSetting({
        returnEdgeFeatures: true,
        returnEdgeGeometry: true,
        returnEdgeIDs: true,
        returnNodeFeatures: true,
        returnNodeGeometry: true,
        returnNodeIDs: true,
        returnPathGuides: true,
        returnRoutes: true
    });
    for (i = 0; i < stops.length; i++) {
        point = new SuperMap.Geometry.Point(stops[i].X, stops[i].Y);
        nodeArray.push(point);
    }

    analystParameter = new SuperMap.REST.TransportationAnalystParameter({
        resultSetting: resultSetting,
        weightFieldName: "length"
    });
    parameter = new SuperMap.REST.FindPathParameters({
        isAnalyzeById: false,
        nodes: nodeArray,
        hasLeastEdgeCount: false,
        parameter: analystParameter
    });
    if (nodeArray.length <= 1) {
        alert("站点数目有误");
    }
    findPathService = new SuperMap.REST.FindPathService(analystUrl, {
        eventListeners: {
            "processCompleted": function(findPathEventArgs) {
                var result = findPathEventArgs.result;
                allScheme(result);
            }
        }
    });
    clearElements()
    findPathService.processAsync(parameter);
}

function allScheme(result) {
    if (pathListIndex < result.pathList.length) {
        addPath(result);
    } else {
        pathListIndex = 0;
        //线绘制完成后会绘制关于路径指引点的信息
        addPathGuideItems(result);
    }
}

//以动画效果显示分析结果
function addPath(result) {
    style = {
        strokeColor: "#304DBE",
        strokeWidth: 3,
        pointerEvents: "visiblePainted",
        fill: false
    }


    if (routeCompsIndex < result.pathList[pathListIndex].route.components[0].components.length) {
        var pathFeature = new SuperMap.Feature.Vector();
        var points = [];
        for (var k = 0; k < 2; k++) {
            if (result.pathList[pathListIndex].route.components[0].components[routeCompsIndex + k]) {
                points.push(new SuperMap.Geometry.Point(result.pathList[pathListIndex].route.components[0].components[routeCompsIndex + k].x, result.pathList[pathListIndex].route.components[0].components[routeCompsIndex + k].y));
            }
        }
        var curLine = new SuperMap.Geometry.LinearRing(points);
        pathFeature.geometry = curLine;
        pathFeature.style = style;
        
        if (map.getLayersByName("route").length == 1) {
            analystLayer = map.getLayersByName("route")[0];
        }
        else{
            analystLayer = new SuperMap.Layer.Vector("route");
            map.addLayer(analystLayer);
        }
        
        analystLayer.addFeatures(pathFeature);
        //每隔0.001毫秒加载一条弧段
        pathTime = setTimeout(function () {
            addPath(result);
        }, 0.001);
        routeCompsIndex++;
    } else {
        clearTimeout(pathTime);
        routeCompsIndex = 0;
        pathListIndex++;
        allScheme(result);
    }
}

function addPathGuideItems(result) {
    styleGuidePoint = {
        pointRadius: 10,
        externalGraphic: "/Content/images/busyCar.png"
    }

    //显示每个pathGuideItem和对应的描述信息
    for (var k = 0; k < result.pathList.length; k++) {
        var pathGuideItems = result.pathList[pathListIndex].pathGuideItems, len = pathGuideItems.length;
        for (var m = 0; m < len; m++) {
            if (pathGuideItems[m].geometry.CLASS_NAME !== "SuperMap.Geometry.Point") {
                continue;
            }
            var guideFeature = new SuperMap.Feature.Vector();
            guideFeature.geometry = pathGuideItems[m].geometry;
            guideFeature.attributes = { description: pathGuideItems[m].description };
            guideFeature.style = styleGuidePoint;
            analystLayer.addFeatures(guideFeature);
        }
    }
}

function clearElements() {
    pathListIndex = 0;
    routeCompsIndex = 0;
    nodeArray = [];
    if (analystLayer)
    analystLayer.removeAllFeatures();
}