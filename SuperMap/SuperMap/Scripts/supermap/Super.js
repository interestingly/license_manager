﻿function Super() {
    this.map = null;
    this.baseurl = null;
    this.polygonpan = null;
    this.rectanglepan = null;
    this.circlepan = null;
    this.modifyPan = null;
    this.nodeArray = [], this.pathTime, this.pathListIndex = 0, this.routeCompsIndex = 0;
    this.analystLayer = null;
    this.baseSpeed = 0.05;
    this.init = function (holder, url, c) {
        this.map = new SuperMap.Map(holder, {
            controls: [
                new SuperMap.Control.LayerSwitcher(),
                new SuperMap.Control.Navigation(),
                new SuperMap.Control.Zoom(),
                new SuperMap.Control.MousePosition()
            ],
            allOverlays: true
        });
        this.baseurl = url;
        baselayer = new SuperMap.Layer.TiledDynamicRESTLayer("baseMap", url, null, { maxResolution: "auto" });
        if (c) {
            center = c;
        }
        m = this.map;
        //监听图层信息加载完成事件
        baselayer.events.on({
            "layerInitialized": function () {
                m.addLayer(baselayer);
                //显示地图范围
                m.setCenter(new SuperMap.LonLat(center.x, center.y), 8);
            }
        });
    }

    this.switchLock = function () {
        var control = this.map.getControlsByClass("SuperMap.Control.Navigation")[0];
        if (control.handlers.wheel.active)
            control.handlers.wheel.deactivate();
        else
            control.handlers.wheel.activate();
    }

    this.showHeatMap = function (points, valuefieldName, radius) {
        radius = radius || 5;
        var heatMapLayer = null;
        lays = this.map.getLayersByName("heatMap");
        if (lays.length == 1)
            heatMapLayer = lays[0];
        else {
            heatMapLayer = new SuperMap.Layer.HeatMapLayer(
                "heatMap",
                {
                    "radius": 5,
                    "featureWeight": valuefieldName,
                    "featureRadius": false
                }
            );
            this.map.addLayer(heatMapLayer);
        }
        heatPoints = [];
        heatMapLayer.removeAllFeatures();
        for (var i = 0; i < points.length; i++) {
            vector = new SuperMap.Feature.Vector(
                new SuperMap.Geometry.Point(points[i].x, points[i].y), points[i]);
            heatPoints.push(vector);
        }
        heatMapLayer.addFeatures(heatPoints);
        heatMapLayer.radius = radius;
    }

    this.showHeatGridMap = function (points, valuefieldName, radius) {
        lays = this.map.getLayersByName("HeatGridLayer");
        if (lays.length == 1)
            heatMapLayer = lays[0];
        else {
            heatMapLayer = new SuperMap.Layer.HeatGridLayer(
                "HeatGridLayer"
            );
            this.map.addLayer(heatMapLayer);
        }
        heatPoints = [];
        var levelstyles = [];
        for (i = 0; i <= 255; i++) {
            var item = {
                start: i,
                end: i + 1,
                style: {
                    strokeColor: "#C69944",
                    strokeWidth: 1,
                    fillColor: "RGB(255," + (255 - i) + "," + (255 - i) + ")",
                    fillOpacity: 0.5
                }
            };
            levelstyles.push(item);
        }
        console.log(levelstyles)


        heatMapLayer.removeAllFeatures();
        for (var i = 0; i < points.length; i++) {
            vector = new SuperMap.Feature.Vector(
                new SuperMap.Geometry.Point(points[i].x, points[i].y), points[i]);
            vector.style = {
                pointRadius:5,
                graphic: true,
                graphicWidth: 16,
                graphicHeight: 16
            }
            heatPoints.push(vector);
        }
        heatMapLayer.spreadZoom = 8;
        //heatMapLayer.isShowLabel = false;
        heatMapLayer.items = levelstyles;
        heatMapLayer.gridWidth = 40;
        heatMapLayer.gridHeight = 40;
        heatMapLayer.addFeatures(heatPoints);
        
    }

    this.showMonitor =function(name,points) {
        var empty = "/Content/images/emptycar.png";
        var busy = "/Content/images/busycar.png";
        var animatorVector = null;
        if (this.map.getLayersByName(name).length == 1)
            animatorVector = this.map.getLayersByName(name)[0];
        else {
            animatorVector = new SuperMap.Layer.Vector(name);
            this.map.addLayer(animatorVector);
        }
        var vectors = [];
        var maxX = 0, maxY = 0, minX = 999999999, minY = 999999999;
        animatorVector.removeAllFeatures();
        for (var i = 0; i < points.length; i++) {
            var p = new SuperMap.Geometry.Point(points[i].X, points[i].Y);
            maxX = points[i].X > maxX ? points[i].X : maxX;
            maxY = points[i].Y > maxY ? points[i].Y : maxY;
            minX = points[i].X < minX ? points[i].X : minX;
            minY = points[i].Y < minY ? points[i].Y : minY;

            var v = new SuperMap.Feature.Vector(p, points[i], {
                graphic: true,
                externalGraphic: points[i].Status ? busy : empty,
                graphicWidth: 15,
                graphicHeight: 15,
                rotation: 30
            });
            vectors.push(v);
        }
        animatorVector.addFeatures(vectors);
        var bound = new SuperMap.Bounds(minX, minY, maxX, maxY);
        this.map.zoomToExtent(bound, true);
    }

    this.showReplay = function(path,onplaying) {
        var animatorVector = null;
        var pathVector = null;
        linestyle = {
            strokeColor: "red",
            strokeWidth: 2,
            fill: false
        };
        var pointstyle =
            {
                
                externalGraphic: "/Content/images/busycar.png",
                allowRotate: true,
                graphicWidth: 15,
                graphicHeight: 15
            };
        if (this.map.getLayersByName("replayLayer").length == 1 && this.map.getLayersByName("pathLayer").length == 1) {
            animatorVector = this.map.getLayersByName("replayLayer")[0];
            pathVector = this.map.getLayersByName("pathLayer")[0];
        }
        else {
            animatorVector = new SuperMap.Layer.AnimatorVector("replayLayer", {}, {
                speed: this.baseSpeed,
                startTime: 0,
                repeat: false
            });
            pathVector = new SuperMap.Layer.Vector("pathLayer");
            this.map.addLayers([pathVector, animatorVector]);
        }
        var route = [];
        var points = [];
        var maxX = 0, maxY = 0, minX = 999999999, minY = 999999999;
        for (var i = 0; i < path.length; i++) {
            maxX = path[i].X > maxX ? path[i].X : maxX;
            maxY = path[i].Y > maxY ? path[i].Y : maxY;
            minX = path[i].X < minX ? path[i].X : minX;
            minY = path[i].Y < minY ? path[i].Y : minY;
            p = new SuperMap.Geometry.Point(path[i].X, path[i].Y)
            points.push(p);
            vector = new SuperMap.Feature.Vector(p.clone(), {
                FEATUREID: path[i].PlateNo,
                //根据节点生成时间
                TIME: path[i].Ticks,
                Extend: path[i]
            }, pointstyle);
            route.push(vector);
        }
        roadLine = new SuperMap.Geometry.LineString(points);
        trail = new SuperMap.Feature.Vector(roadLine, null, linestyle);
        pathVector.removeAllFeatures();
        animatorVector.removeAllFeatures();
        pathVector.addFeatures([trail]);
        animatorVector.addFeatures(route);
        animatorVector.animator.setStartTime(path[0].Ticks);
        animatorVector.animator.setSpeed(this.baseSpeed);
        if (onplaying != null)
            animatorVector.events.on({ "featurerendered": onplaying });
        animatorVector.animator.start();
        var bound = new SuperMap.Bounds(minX, minY, maxX, maxY);
        this.map.zoomToExtent(bound, true);
        return animatorVector;
    }
    
    this.setSpeed = function (animator, n) {
        animator.animator.setSpeed(this.baseSpeed * n);
    }

    this.setCurrent = function (animator, ticks) {
        animator.animator.setCurrentTime(ticks);
    }

    this.pausePlay = function (animator) {
        animator.animator.pause();
    }

    this.stopPlay = function (animator) {
        animator.animator.stop();
    }

    this.appendLayer=function(name, url) {
        var layer = null;
        if (this.map.getLayersByName(name).length == 1) {
            layer = this.map.getLayersByName(name)[0];
            layer.url = url;
            layer.setVisibility(true);
        }
        else {
            layer = new SuperMap.Layer.TiledDynamicRESTLayer(name, url, { transparent: true, cacheEnabled: true });
            layer.setOpacity(1);
            m = this.map;
            layer.events.on({
                "layerInitialized": function () {
                    m.addLayer(layer);
                }
            });
        }
    }

    this.hideLayer = function(name) {
        var layer = null;
        if (this.map.getLayersByName(name).length == 1) {
            layer = this.map.getLayersByName(name)[0];
            this.map.removeLayer(layer,false)
        }
    }

    this.tiggerRails = function (name,rails) {
        var layer = null;
        var standStyle = {
            strokeColor: "#CAFF70",
            fillColor: "#C6E2FF",
            strokeWidth: 2,
            fillOpacity: 0.5
        };
        if (this.map.getLayersByName(name).length == 1) {
            layer = this.map.getLayersByName(name)[0];
        }
        else {
            layer = new SuperMap.Layer.Vector(name);
            this.map.addLayer(layer);
        }
        var vectors = [];
        for (i = 0; i < rails.length; i++) {
            var label = rails[i].label;
            if (rails[i].type == "Circle") {
                var centerPoint = new SuperMap.Geometry.Point(rails[i].center.X, rails[i].center.Y);
                var circleP = this.createCircle(centerPoint, rails[i].radius, 256, 360, 360);
                var style = jQuery.extend(true, {}, standStyle);
                style.label = label;
                var circleVector = new SuperMap.Feature.Vector(circleP, null, style);
                vectors.push(circleVector);
            }
            else {
                var path = [];
                trail = rails[i].path;
                for (j = 0; j < trail.length; j++) {
                    point = new SuperMap.Geometry.Point(trail[j].X, trail[j].Y);
                    path.push(point);
                }
                linearRings = new SuperMap.Geometry.LinearRing(path);
                region = new SuperMap.Geometry.Polygon([linearRings]);
                var style = jQuery.extend(true, {}, standStyle);
                style.label = label;
                polygonVector = new SuperMap.Feature.Vector(region, null, style);
                vectors.push(polygonVector);
            }
        }
        layer.removeAllFeatures();
        layer.addFeatures(vectors);
        return layer;
    }

    this.supportClick2GetLocation = function () {
        layer = null;
        if (this.map.getLayersByName("test").length == 1) {
            layer = this.map.getLayersByName("test")[0];
        }
        else {
            layer = new SuperMap.Layer.Vector("test");
            this.map.addLayer(layer);
        }
        this.map.events.on({
            "click": function (args) {
                px = new SuperMap.Pixel(args.layerX, args.layerY);
                lnglat = this.map.getLonLatFromPixel(px);
                point = new SuperMap.Geometry.Point(lnglat.lon, lnglat.lat);
                console.log(lnglat);
                vector = new SuperMap.Feature.Vector(point);
                layer.addFeatures([vector]);
                //$.post("/Home/CollectPoints", { location: lnglat.toShortString() }, function (data) {

                //});

            }
        })
    }

    this.createCircle = function (origin, radius, sides, r, angel) {
        var rR = r * Math.PI / (180 * sides);
        var rotatedAngle, x, y;
        var points = [];
        for (var i = 0; i < sides; ++i) {
            rotatedAngle = rR * i;
            x = origin.x + (radius * Math.cos(rotatedAngle));
            y = origin.y + (radius * Math.sin(rotatedAngle));
            points.push(new SuperMap.Geometry.Point(x, y));
        }
        rotatedAngle = r * Math.PI / 180;
        x = origin.x + (radius * Math.cos(rotatedAngle));
        y = origin.y + (radius * Math.sin(rotatedAngle));
        points.push(new SuperMap.Geometry.Point(x, y));

        var ring = new SuperMap.Geometry.LinearRing(points);
        ring.rotate(parseFloat(angel), origin);
        var geo = new SuperMap.Geometry.Collection([ring]);
        geo.origin = origin;
        geo.radius = radius;
        geo.r = r;
        geo.angel = angel;
        geo.sides = sides;
        geo.polygonType = "Curve";
        return geo;
    }

    this.addDrawControllers = function () {
        vectorlayer = null;
        if (this.map.getLayersByName("DrawingLayer").length == 1)
            vectorlayer = this.map.getLayersByName("DrawingLayer")[0];
        else {
            vectorlayer = new SuperMap.Layer.Vector("DrawingLayer");
            this.map.addLayer(vectorlayer);
        }

        this.polygonpan = new SuperMap.Control.DrawFeature(vectorlayer, SuperMap.Handler.Polygon);
        this.rectanglepan = new SuperMap.Control.DrawFeature(vectorlayer, SuperMap.Handler.Box);
        this.circlepan = new SuperMap.Control.DrawFeature(vectorlayer, SuperMap.Handler.RegularPolygon, { handlerOptions: { sides: 360 } });
        this.modifyPan = new SuperMap.Control.ModifyFeature(vectorlayer);
        this.modifyPan.snap = new SuperMap.Snap([vectorlayer], 10, 10, { actived: false })
        this.map.addControls([this.polygonpan, this.rectanglepan, this.circlepan, this.modifyPan]);
    }

    this.drawPolygon = function (callback) {
        this.disableDrawing();
        this.polygonpan.events.on({ "featureadded": callback });
        this.polygonpan.activate();
    }

    this.drawRectangle = function (callback) {
        this.disableDrawing()
        this.rectanglepan.events.on({ "featureadded": callback });
        this.rectanglepan.activate();
    }

    this.drawCircle = function (callback) {
        this.disableDrawing()
        this.circlepan.events.on({ "featureadded": callback });
        this.circlepan.activate();
    }

    this.disableDrawing = function () {
        this.polygonpan.deactivate();
        this.rectanglepan.deactivate();
        this.circlepan.deactivate();
        this.modifyPan.deactivate();
        this.modifyPan.snap.off();
    }

    this.enbaleEdit = function (before, doing, after) {
        this.disableDrawing();
        this.modifyPan.events.on({ "beforefeaturemodified": before });
        this.modifyPan.events.on({ "featuremodified": doing });
        this.modifyPan.events.on({ "afterfeaturemodified": after });
        this.modifyPan.activate();
        this.modifyPan.snap.on();
    }

    this.PoiSearch = function (keyword, callback, url) {
        PoiStyle = {
            graphic: true,
            externalGraphic: "/Content/images/poimarker.png",
            graphicWidth: 20,
            graphicHeight: 25,
            graphicYOffset: -20
        }

        vectorLayer = null;
        if (this.map.getLayersByName("poiSearch").length == 1) {
            vectorLayer = this.map.getLayersByName("poiSearch")[0];
            vectorLayer.removeAllFeatures();
        }
        else {
            vectorLayer = new SuperMap.Layer.Vector("poiSearch");
            this.map.addLayer(vectorLayer);
        }

        vectorLayer.removeAllFeatures();

        var queryParams = [], queryBySQLParams, queryBySQLService;
        queryParams[0] = new SuperMap.REST.FilterParameter({
            name: "Airport_pt@China.1",
            attributeFilter: "Name like '%" + keyword + "%'"
        });
        queryBySQLParams = new SuperMap.REST.QueryBySQLParameters({
            queryParams: queryParams
        });
        var processCompleted = this.processCompleted;
        queryBySQLService = new SuperMap.REST.QueryBySQLService(url, {
            eventListeners: {
                "processCompleted": callback ? callback : processCompleted, "processFailed": function processFailed(e) {
                    alert(e.error.errorMsg);
                }
            }
        });
        queryBySQLService.processAsync(queryBySQLParams);
    }

    this.processCompleted = function (queryEventArgs) {
        var i, j, feature,
            result = queryEventArgs.result;
        if (result && result.recordsets) {
            for (i = 0; i < result.recordsets.length; i++) {
                if (result.recordsets[i].features) {
                    for (j = 0; j < result.recordsets[i].features.length; j++) {
                        feature = result.recordsets[i].features[j];
                        feature.style = PoiStyle;
                        vectorLayer.addFeatures(feature);
                    }
                }
            }
        }
    }

    this.routeSearch = function (stops,serviceUrl) {
        var findPathService, parameter, analystParameter, resultSetting;
        resultSetting = new SuperMap.REST.TransportationAnalystResultSetting({
            returnEdgeFeatures: true,
            returnEdgeGeometry: true,
            returnEdgeIDs: true,
            returnNodeFeatures: true,
            returnNodeGeometry: true,
            returnNodeIDs: true,
            returnPathGuides: true,
            returnRoutes: true
        });
        for (i = 0; i < stops.length; i++) {
            point = new SuperMap.Geometry.Point(stops[i].X, stops[i].Y);
            this.nodeArray.push(point);
        }

        analystParameter = new SuperMap.REST.TransportationAnalystParameter({
            resultSetting: resultSetting,
            weightFieldName: "LENGTH"
        });
        parameter = new SuperMap.REST.FindPathParameters({
            isAnalyzeById: false,
            nodes: this.nodeArray,
            hasLeastEdgeCount: true,
            parameter: analystParameter
        });
        if (this.nodeArray.length <= 1) {
            alert("站点数目有误");
        }
        var that = this;
        findPathService = new SuperMap.REST.FindPathService(serviceUrl, {
            eventListeners: {
                "processCompleted": function (findPathEventArgs) {
                    var result = findPathEventArgs.result;
                    console.log(result);
                    that.allScheme(result);
                }
            }
        });
        this.clearElements()
        findPathService.processAsync(parameter);
    }

    this.allScheme = function (result) {
        if (this.pathListIndex < result.pathList.length) {
            this.addPath(result);
        } else {
            this.pathListIndex = 0;
            //线绘制完成后会绘制关于路径指引点的信息
            this.addPathGuideItems(result);
        }
    }

    this.addPath = function (result) {
        style = {
            strokeColor: "#304DBE",
            strokeWidth: 3,
            pointerEvents: "visiblePainted",
            fill: false
        }


        if (this.routeCompsIndex < result.pathList[this.pathListIndex].route.components[0].components.length) {
            var pathFeature = new SuperMap.Feature.Vector();
            var points = [];
            for (var k = 0; k < 2; k++) {
                if (result.pathList[this.pathListIndex].route.components[0].components[this.routeCompsIndex + k]) {
                    points.push(new SuperMap.Geometry.Point(result.pathList[this.pathListIndex].route.components[0].components[this.routeCompsIndex + k].x, result.pathList[this.pathListIndex].route.components[0].components[this.routeCompsIndex + k].y));
                }
            }
            var curLine = new SuperMap.Geometry.LinearRing(points);
            pathFeature.geometry = curLine;
            pathFeature.style = style;

            if (this.map.getLayersByName("route").length == 1) {
                this.analystLayer = this.map.getLayersByName("route")[0];
            }
            else {
                this.analystLayer = new SuperMap.Layer.Vector("route");
                this.map.addLayer(this.analystLayer);
            }
            var that = this;
            this.analystLayer.addFeatures(pathFeature);
            //每隔0.001毫秒加载一条弧段
            this.pathTime = setTimeout(function () {
                that.addPath(result);
            }, 0.001);
            this.routeCompsIndex++;
        } else {
            window.clearTimeout(this.pathTime);
            this.routeCompsIndex = 0;
            this.pathListIndex++;
            this.allScheme(result);
        }
    }

    this.addPathGuideItems = function (result) {
        styleGuidePoint = {
            pointRadius: 10,
            externalGraphic: "/Content/images/busyCar.png"
        }

        //显示每个pathGuideItem和对应的描述信息
        for (var k = 0; k < result.pathList.length; k++) {
            var pathGuideItems = result.pathList[this.pathListIndex].pathGuideItems, len = pathGuideItems.length;
            for (var m = 0; m < len; m++) {
                if (pathGuideItems[m].geometry.CLASS_NAME !== "SuperMap.Geometry.Point") {
                    continue;
                }
                var guideFeature = new SuperMap.Feature.Vector();
                guideFeature.geometry = pathGuideItems[m].geometry;
                guideFeature.attributes = { description: pathGuideItems[m].description };
                guideFeature.style = styleGuidePoint;
                this.analystLayer.addFeatures(guideFeature);
            }
        }
    }

    this.clearElements = function () {
        this.pathListIndex = 0;
        this.routeCompsIndex = 0;
        this.nodeArray = [];
        if (this.analystLayer)
            this.analystLayer.removeAllFeatures();
    }

    this.showRegion = function (layerName, param, display, after) {
        var vectorLayer = null;
        var m = this.map;
        var layerIndex = 1;
        style = {
            strokeColor: "#000000",
            strokeWidth: 1,
            fillColor: "#F8CBF9",
            fillOpacity: "0.3"
        }
        if (m.getLayersByName(layerName).length == 1) {
            vectorLayer = m.getLayersByName(layerName)[0];
            layerIndex = m.getLayerIndex(vectorLayer);
            m.removeLayer(vectorLayer, true);
            if (display == false) {
                return;
            }

        }
        var queryParam, queryBySQLParams, queryBySQLService;
        if (param.city != null) {
            queryParam = new SuperMap.REST.FilterParameter({
                name: "广东市面@GD",
                attributeFilter: "DISTNAME='" + param.city + "'"
            });
        } else if (param.district != null) {
            queryParam = new SuperMap.REST.FilterParameter({
                name: "广东县面@GD",
                attributeFilter: "DISTNAME='" + param.district + "'"
            });
        } else {
            queryParam = new SuperMap.REST.FilterParameter({
                name: "广东省面@GD",
                attributeFilter: "DISTNAME='" + param.province + "'"
            });
        }
        queryBySQLParams = new SuperMap.REST.QueryBySQLParameters({
            queryParams: [queryParam]
        });
        queryBySQLService = new SuperMap.REST.QueryBySQLService(this.baseurl + "/iserver/services/map-gd/rest/maps/GD_夜景", {
            eventListeners: {
                "processCompleted": function (queryEventArgs) {
                    var i, j, feature,
                        result = queryEventArgs.result;
                    vectorLayer = new SuperMap.Layer.Vector("区县图");
                    if (result && result.recordsets) {
                        for (i = 0; i < result.recordsets.length; i++) {
                            if (result.recordsets[i].features) {
                                for (j = 0; j < result.recordsets[i].features.length; j++) {
                                    feature = result.recordsets[i].features[j];
                                    console.log(feature);//查找的地域区块
                                    feature.style = style;
                                    vectorLayer.addFeatures(feature);
                                    m.addLayer(vectorLayer, layerIndex);
                                    m.zoomToExtent(feature.geometry.getBounds(), true);
                                }
                            }
                        }
                    }
                    if (after != null)
                        after();

                }, "processFailed": function (e) {
                    alert("未找到相关市县");
                }
            }
        });
        queryBySQLService.processAsync(queryBySQLParams);

    }



    this.what = function () {
        console.log(this);
    }
    


}