﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace SuperMap.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SubmitForm(Models.Form form)
        {
            Utility.Geocoder geo = new Utility.Geocoder();
            form.start = geo.LngLat2Address(form.startpoi);
            form.back = geo.LngLat2Address(form.backpoi);
            using (var conn = new MySqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LocalMySQL"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO intention (`Submiter`, `tel`, `village`, `worktime`, `backtime`, `start`, `back`, `startpoi`, `backpoi`) VALUES (?submiter, ?tel, ?village, ?worktime, ?backtime, ?start, ?back, ?startpoi, ?backpoi)";
                try
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("?submiter", form.submiter);
                    cmd.Parameters.AddWithValue("?tel", form.tel);
                    cmd.Parameters.AddWithValue("?village", form.village);
                    cmd.Parameters.AddWithValue("?worktime", form.worktime);
                    cmd.Parameters.AddWithValue("?backtime", form.backtime);
                    cmd.Parameters.AddWithValue("?start", form.start);
                    cmd.Parameters.AddWithValue("?back", form.back);
                    cmd.Parameters.AddWithValue("?startpoi", JsonConvert.SerializeObject(form.startpoi));
                    cmd.Parameters.AddWithValue("?backpoi", JsonConvert.SerializeObject(form.backpoi));

                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    throw;
                }


            }



            return Content("提交成功");
        }

        public ActionResult CheckTelExist(string tel)
        {
            using (var conn = new MySqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LocalMySQL"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandText = "select count(1) from intention where tel=?tel";
                try
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("?tel", tel);

                    var count = Convert.ToInt16(cmd.ExecuteScalar());
                    if (count > 0)
                        return Content("手机号已经登记,请不要重复登记");
                    else
                        return Content("");
                }
                catch
                {
                    throw;
                }
            }
        }
    }
}